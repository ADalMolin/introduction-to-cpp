// Andrea Dal Molin
#ifndef RE_PARAMETERS_H
#define RE_PARAMETERS_H

// ------------- Define physical constants -------------
extern const float c;                                                          // Light speed in vacuum [m/s]
extern const float m_e;                                                        // Electron rest mass [kg]
extern const float q_e;                                                        // Electron electric charge [C]
extern const float eps_0;                                                      // Vacuum permittivity [F/m]

// ------------- Simulation parameters -------------
extern float dtau;                                                              // Temporal step [s]
extern int iterations;                                                          // Number of iterations
extern float q_par_max;                                                        // Maximum value of the normalized parallel momentum
extern float q_par_min;                                                        // Minimum value of the normalized parallel momentum
extern float q_per_max;                                                        // Maximum value of the normalized perpendicular momentum
extern float q_per_min;                                                        // Minimum value of the normalized perpendicular momentum

// ------------- Machine parameters -------------
extern float B_0;                                                              // Magnetic field at the magnetic axis [T]
extern float R_0;                                                              // Major radius of the machine [m]

// ------------- Plasma parameters -------------
extern float D;                                                                // E_parallel / E_critical
extern float alpha;                                                            // Z effective charge (atomic number NOT ionization states)
extern float ln_Lam;                                                           // Coulomb logarithm
extern float n_e;                                                              // Electron density [m-3]

// ------------- Plasma chatacteristics -------------
extern float nu_e;                                                             // Collisional frequency [s-1]
extern float F_gy;                                                             // Synchrotron gyromotion factor
extern float F_gc;                                                             // Syncrhotron guiding center factor
extern float F_br;                                                             // Bremsstrahlung factor

#endif
