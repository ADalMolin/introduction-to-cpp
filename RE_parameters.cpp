// Andrea Dal Molin
#include "RE_parameters.h"
#include <math.h>

// ------------- Define physical constants -------------
const float c = 299792458;                                                     // Light speed in vacuum [m/s]
const float m_e = 9.10938356e-31;                                              // Electron rest mass [kg]
const float q_e = -1.6021766208e-19;                                           // Electron electric charge [C]
const float eps_0 = 8.85418781762e-12;                                         // Vacuum permittivity [F/m]

// ------------- Simulation parameters -------------
float dtau = 1e-2;                                                              // Temporal step [s]
int iterations = int(1e5);                                                      // Number of iterations
float q_par_max = +15.0;                                                       // Maximum value of the normalized parallel momentum
float q_par_min = -15.0;                                                       // Minimum value of the normalized parallel momentum
float q_per_max = +6.00;                                                       // Maximum value of the normalized perpendicular momentum
float q_per_min = +0.40;                                                       // Minimum value of the normalized perpendicular momentum

// ------------- Machine parameters -------------
float B_0 = 4.0;                                                               // Magnetic field at the magnetic axis [T]
float R_0 = 2.0;                                                               // Major radius of the machine [m]

// ------------- Plasma parameters -------------
float D = 4.4;                                                                 // E_parallel / E_critical
float alpha = 4;                                                               // Z effective charge (atomic number NOT ionization states)
float ln_Lam = 20;                                                             // Coulomb logarithm
float n_e = 1e19;                                                              // Electron density [m-3]

// ------------- Plasma chatacteristics -------------
float nu_e = n_e*(pow(q_e,4))*ln_Lam/(4*M_PI*(pow(eps_0,2))*(pow(m_e,2))*(pow(c,3))); // Collisional frequency [s-1]
float F_gy = (2*eps_0*pow(B_0,2))/(3*n_e*ln_Lam*m_e);                          // Synchrotron gyromotion factor
float F_gc = F_gy*pow(m_e*c/(q_e*B_0*R_0),2);                                  // Syncrhotron guiding center factor
float F_br = 1./(137*M_PI*ln_Lam);                                             // Bremsstrahlung factor
