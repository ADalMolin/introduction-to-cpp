// Ansrea Dal Molin
#include "RE_functions.h"
#include "RE_parameters.h"
#include <math.h>
#include <iostream>

using namespace REFunc;
using namespace std;

// Returns the time derivative in the momentum space
tuple<float, float> REF::q_dot(float q_par, float q_per){
  float q = sqrt(pow(q_par,2) + pow(q_per,2));   // Nomalized momentum norm
  float gamma = sqrt(1 + pow(q,2));              // Relativistic factor
  float v_c = sqrt(pow(gamma,2) -1)/gamma;       // v/c, v electron velocity

  float q_par_dot = D - gamma*(alpha+gamma)*q_par/(pow(q,3)) - (F_gc+F_gy*(pow(q_per,2))/pow(q,4))*(pow(gamma,4))*(pow(v_c,3))*q_par/q - F_br*alpha*gamma*(gamma*log(2)-1./3)*q_par/q;
  float q_per_dot = gamma*(alpha+gamma)*(pow(q_par,2))/(q_per*(pow(q,3))) - (pow(gamma,2))/(q*q_per) - (F_gc+F_gy*(pow(q_per,2))/pow(q,4))*(pow(gamma,4))*(pow(v_c,3))*q_per/q - F_br*alpha*gamma*(gamma*log(2)-1./3)*q_per/q;

  return make_tuple(q_par_dot, q_per_dot);
};

// Returns particle coordinates in the energy - pitch angle space
tuple<float, float> REF::from_q_to_E_mu(float q_par, float q_per){
  float q = sqrt(pow(q_par,2) + pow(q_per,2));    // Nomalized momentum norm
  float gamma = sqrt(1 + pow(q,2));               // Relativistic factor
  float K = m_e*(pow(c,2))*(gamma-1)/q_e;         // Energy from J to eV
  float mu = q_par/q;                             // Pitch angle

  return make_tuple(K, mu);
};

// Returns particle coordinates in the momentum space
tuple<float, float> REF::from_E_mu_to_q(float K, float mu){
  float gamma = q_e*K/(m_e*(pow(c,2))) + 1;       // Relativistic factor (energy in eV)
  float q = sqrt(pow(gamma,2) - 1);               // Nomalized momentum norm
  float q_par = mu*q;                             // Nomalized parallel momentum
  float q_per = sqrt(pow(q,2) - pow(q_par,2));    // Nomalized perpendicular momentum

  return make_tuple(q_par, q_per);
};

// Simulates particle evolution in phase space and returns particle lore
vector<boost::tuple<float, float>> REF::particle_evolution(float q_par_init, float q_per_init){
  std::vector<boost::tuple<float, float> > lore;
  float q_par=q_par_init, q_per=q_per_init;
  float q_par_increment, q_per_increment;
  lore.push_back(boost::make_tuple(q_par, q_per)); // Stores inital coordinates

  for(int i=0; i<iterations; i++){
    tie(q_par_increment, q_per_increment) = REF::q_dot(q_par,q_per);  // Computes increments
    q_par += q_par_increment*dtau;                                    // Updates variables
    q_per += q_per_increment*dtau;
    lore.push_back(boost::make_tuple(q_par, q_per));                  // Stores new coordinates
  }
  return lore;
};

// Plot momentum space
vector<boost::tuple<float, float, float, float>> REF::momentum_space(){
  std::vector<boost::tuple<float, float, float, float>> v_field;   // Stores each vector of the vector field

  for(float i = q_par_min; i <= q_par_max; i += (q_par_max-q_par_min)/101){
    for(float j = q_per_min; j <= q_per_max; j += (q_per_max-q_per_min)/101){
      float q_par_increment, q_per_increment;
      tie(q_par_increment, q_per_increment) = REF::q_dot(i,j);
      v_field.push_back(boost::make_tuple(i, j,	q_par_increment*dtau, q_per_increment*dtau));
    }
  }
  return v_field;
};
