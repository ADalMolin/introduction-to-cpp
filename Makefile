# Makefile to compile the RE programm

# *****************************************************
# Variables to control Makefile operation

CXX = g++
CXXFLAGS1 = -std=c++11
CXXFLAGS2 = -lboost_iostreams -lboost_system -lboost_filesystem

# ****************************************************
# Targets needed to bring the executable up to date
RE_phase_space: RE_phase_space.o RE_functions.o RE_parameters.o
	$(CXX) $(CXXFLAGS1) -o RE_phase_space RE_phase_space.o RE_functions.o RE_parameters.o $(CXXFLAGS2)

RE_phase_space.o: RE_phase_space.cpp RE_functions.h RE_parameters.h
	$(CXX) $(CXXFLAGS1) -c RE_phase_space.cpp

RE_functions.o:	RE_functions.h RE_parameters.h
	$(CXX) $(CXXFLAGS1) -c RE_functions.cpp

RE_parameters.o: RE_parameters.h
	$(CXX) $(CXXFLAGS1) -c RE_parameters.cpp
