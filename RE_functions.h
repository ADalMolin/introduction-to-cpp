// Andrea Dal Molin
#ifndef RE_FUNCTIONS_H
#define RE_FUNCTIONS_H

#include <tuple>
#include <boost/tuple/tuple.hpp>

namespace REFunc
{
    class REF
    {
    public:
        // Returns the time derivative in the momentum space
        static std::tuple<float, float> q_dot(float q_par, float q_per);

        // Returns particle coordinates in the energy - pitch angle space
        static std::tuple<float, float> from_q_to_E_mu(float q_par, float q_per);

        // Returns particle coordinates in the momentum space
        static std::tuple<float, float> from_E_mu_to_q(float K, float mu);

        // Simulates particle evolution in phase space and returns particle lore
        static std::vector<boost::tuple<float, float>> particle_evolution(float q_par_init, float q_per_init);

        // Plot momentum space
        static std::vector<boost::tuple<float, float, float, float>> momentum_space();
    };
}

#endif
