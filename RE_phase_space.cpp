// Andrea Dal Molin
// This programm computes the runaway electron phase space. For more information see:
// 1) G. Fussmann 1979 Nucl. Fusion 19 327;
// 2) J. R. Martin-Solis et al. 1998 Phys. Plasmas 5, 2370
// 3) M. Bakhtiari et al 2005 Phys. Plasmas 12, 102503
// 4) I Fernandez-Gomez et al. 2007 Phys. Plasmas 14, 072503

#include <iostream>
#include "RE_parameters.h"
#include "RE_functions.h"
#include "gnuplot-iostream.h"

using namespace REFunc;
using namespace std;

float q_par, q_per;

int main()
{
  Gnuplot gp;

  // Print radiation factors
  cout << "Radiation factors:" << endl;
  cout << "F_gy: " << F_gy << endl;
  cout << "F_gc: " << F_gc << endl;
  cout << "F_br: " << F_br << endl;

  // Particle starting energy [eV] and pitch angle as cos(theta)
  tie(q_par,q_per) = REF::from_E_mu_to_q(4e6,0.6);

  // Compute phase space and particle evolution
  vector<boost::tuple<float, float, float, float>> v_field = REF::momentum_space();
  vector<boost::tuple<float, float>> lore = REF::particle_evolution(q_par, q_per);

  // Plot results
  gp << "set xrange [" << q_par_min << ":" << q_par_max << "]\nset yrange [" << q_per_min << ":" << q_per_max << "]\n";
  gp << "set xlabel 'q_par'\nset ylabel 'q_per'\n";
  gp << "plot '-' with vectors title 'RE phase space', '-' with lines lt rgb 'blue' title 'Particle evolution'\n";
  gp.send1d(v_field);
  gp.send1d(lore);
}
